public class Payroll {
    private String name;
    private int employeeNum;
    private double payRate;
    private int numOfHours;

    Payroll(String name, int employeeNum)
    {
        this.name = name;
        this.employeeNum = employeeNum;
    }

    public String getName()
    {
        return name;
    }

    public int getEmployeeNum()
    {
        return employeeNum;
    }

    public double getPayRate()
    {
        return payRate;
    }

    public int getNumOfHours()
    {
        return numOfHours;
    }

    public void setEmployeeNum(int employeeNum) {
        this.employeeNum = employeeNum;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setNumOfHours(int numOfHours) {
        this.numOfHours = numOfHours;
    }

    public void setPayRate(double payRate) {
        this.payRate = payRate;
    }

    public double getGrossPay()
    {
        return numOfHours*payRate;
    }
}
