import java.util.Scanner;

public class main {
    public static void main (String args[])
    {
        Scanner k = new Scanner(System.in);
        System.out.println("What is your name?");
        String name = k.next();
        System.out.println("What is your employee number?");
        int num = k.nextInt();
        Payroll payroll = new Payroll(name, num);
        System.out.println("What is your hourly wage?");
        payroll.setPayRate(k.nextDouble());
        System.out.println("How many hours have you worked?");
        payroll.setNumOfHours(k.nextInt());
        System.out.println("You are owed $c" + payroll.getGrossPay());
    }
}
